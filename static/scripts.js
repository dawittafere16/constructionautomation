function toggleNavMenu() {
    var navLinks = document.getElementById("navLinks");
    navLinks.classList.toggle("show-menu");
}

// Function to set the theme switch state based on the stored theme preference
function setThemeSwitch() {
    const themeSwitch = document.getElementById("themeSwitch");
    const isDarkTheme = localStorage.getItem("isDarkTheme") === "true";
    const isLightTheme = localStorage.getItem("isLightTheme") === "true";
    if(isDarkTheme)
    {
    themeSwitch.checked = isDarkTheme;
    }
    else
    {
      themeSwitch.checked = isLightTheme
    }
}


// Call setThemeSwitch() when the DOM is ready
document.addEventListener("DOMContentLoaded", setThemeSwitch);

 // JavaScript to add "current-page" class to the appropriate link based on the current page URL
 document.addEventListener('DOMContentLoaded', function() {
    const navLinks = document.querySelectorAll('.nav-links li a');
    const currentPage = window.location.pathname;

    console.log('current page',currentPage)
  
    for (const link of navLinks) {
      if (link.getAttribute('href') === currentPage) {
        link.classList.add('current-page');
      }
    }
  });

// Function to toggle between dark and light themes
function toggleTheme() {
    const body = document.body;
  
    const isGradientTheme = body.classList.contains("dark-theme");

    // If the current theme is 'gradient-theme', switch to 'dark-theme'
    if (isGradientTheme) {
      body.classList.remove("dark-theme");
      body.classList.add("light-theme");
      const isDarkTheme = body.classList.contains("Light-theme");
      localStorage.removeItem("isDarkTheme")
      localStorage.setItem("isLightTheme", isDarkTheme);
    } else { // If the current theme is 'dark-theme', switch to 'gradient-theme'
      body.classList.remove("light-theme");
      body.classList.add("dark-theme");
          // Store the current theme preference in local storage
    const isLightTheme = body.classList.contains("dark-theme");
    localStorage.removeItem("isLightTheme")
    localStorage.setItem("isdarkTheme", isLightTheme);
    }
  
   

    // Toggle the displayed icon based on the selected theme
    const lightIcon = document.querySelector(".sun-icon");
    const darkIcon = document.querySelector(".moon-icon");
    lightIcon.classList.toggle("hide");
    darkIcon.classList.toggle("hide");


}

// dark-mode.js

// Function to set the theme on page load
function setTheme() {
    const isDarkTheme = localStorage.getItem("isDarkTheme") === "true";
    if (isDarkTheme) {
        const body = document.body;
        body.classList.remove("light-theme")
        body.classList.add("dark-theme");

        // Toggle the displayed icon based on the selected theme
        const lightIcon = document.querySelector(".sun-icon");
        const darkIcon = document.querySelector(".moon-icon");
        lightIcon.classList.add("hide");
        darkIcon.classList.remove("hide");
    }
    else
    {
      const body = document.body;
      body.classList.remove("dark-theme")
      body.classList.add("light-theme");
    }
}

// Set the theme on page load
setTheme();

// // Event listener to handle theme toggle when the switch is clicked
// document.getElementById("themeSwitch").addEventListener("click", toggleTheme);





function submitForm() {
  var bulkhead = $("#bulkhead").val();
  var WDb = $("#wDb").val();
  var sheetingD = $("#sheetingD").val();
  var pileD = $("#pileD").val();
  var tidalR = $("#tidalR").val();
  var stormS = $("#stormS").val();
  var rainF = $("#rainF").val();
  var soil = $("#soil").val();
  var email = $("#email").val();
  var form = document.getElementById('form')
  var info = document.getElementById('info')
  // Get values of other input fields similarly

  var loader = $("#loader");

  loader.css("display", "flex");
  
  $.ajax({
      type: "POST",
      url: "/submit",
      data: JSON.stringify({
          bulkhead: bulkhead,
          wDb: WDb,
          sheetingD: sheetingD,
          pileD: pileD,
          tidalR: tidalR,
          stormS: stormS,
          rainF: rainF,
          soil: soil,
          email: email,
          // Add other input fields to the data object
      }),
      contentType: "application/json",
      success: function (response) {
          // alert("Calculation submitted successfully.");
          form.reset();
          loader.css("display", "none");
          window.location.href = response.thank_you_url
      },
      error: function (error) {
          alert("An error occurred while processing the request.");
          loader.css("display", "none");
          info.style.display = 'block';
      },
  });
}


// loader on page routing

document.addEventListener('DOMContentLoaded', function() {
  // Show the loader when clicking on links to other pages
  $('a').click(function(event) {
      var href = $(this).attr('href');
      if (href !== '#' && !href.startsWith('#')) { // Exclude anchor links and other special links
          event.preventDefault(); // Prevent default link behavior
          showLoader(); // Show the loader

          // After a small delay (optional), navigate to the clicked page
          setTimeout(function() {

              window.location.href = href;
          }, 2800); // Adjust the delay time as needed (in milliseconds)
      }
  });
});

// Function to show the loader
function showLoader() {
  var loader = $("#loader");
  loader.css("display", "flex"); // You can customize the loader animation here if needed
}

// Function to hide the loader
function hideLoader() {
  var loader = $("#loader");
  loader.css("display", "none");// You can customize the loader animation here if needed
}